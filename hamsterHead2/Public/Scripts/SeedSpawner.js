// -----JS CODE-----

//@input SceneObject mouthTarget
//@input Asset.ObjectPrefab seedPrefab
//@input SceneObject mouthTarget
//@input bool spawnInitialSeed
//@input float seedSpawnTime
//@input float distScaleX
//@input float distScaleY
//@input float distScaleZ
//@input float distOffsetZ
//@input int maxNumSeeds

global.numSeeds = 0;
var seedSpawnTimer = 0;
var event = script.createEvent("UpdateEvent");
var robotSqueak = script.mouthTarget.getFirstComponent("Component.AudioComponent");

// Spawn seed every interval if number of seeds is below thresh
function update(eventData)
{
    seedSpawnTimer += getDeltaTime();
    if (seedSpawnTimer > script.seedSpawnTime && global.numSeeds < script.maxNumSeeds)
    {
        seedSpawnTimer = 0;
        if (script.seedPrefab)
        {
            spawnSeed();
        }
    }
    else if (script.spawnInitialSeed && global.numSeeds == 0)
    {
        spawnSeed();
    }
}
event.bind(update);

// Instantiate seed, set position, and increment counter
function spawnSeed()
{
    var seedObject = script.seedPrefab.instantiate(script.getSceneObject());
    // Set position over normal distribution (done here to avoid weird jittering later)
    var pos = script.mouthTarget.getTransform().getWorldPosition();
    pos.x += script.distScaleX * randBoxMuller();
    pos.y += script.distScaleY * randBoxMuller();
    pos.z += script.distOffsetZ + script.distScaleZ * randBoxMuller();
    seedObject.getTransform().setWorldPosition(pos);
    global.numSeeds += 1;
    print("seed spawned");
}

// called by seed when destroyed to prevent seeds from respawning instantly (annoying)
script.api.onSeedEaten = function()
{
    seedSpawnTimer = 0;
    robotSqueak.play(1);
}

// Standard Normal distribution using Box-Muller transform.
// https://stackoverflow.com/questions/25582882/javascript-math-random-normal-distribution-gaussian-bell-curve
function randBoxMuller() {
    var u = 0, v = 0;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    return Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
}