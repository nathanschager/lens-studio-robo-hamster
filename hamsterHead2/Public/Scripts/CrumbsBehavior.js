// -----JS CODE-----
//@input float lifetime
//@input Asset.Material crumbsMaterial

// Re-clone crumbs material to reset particle timer
var meshVis = script.getSceneObject().getFirstComponent("Component.RenderMeshVisual");
var startTime = getTime();
var crumbsMatClone = script.crumbsMaterial.clone();
meshVis.addMaterial(script.crumbsMaterial);

// Change particle simulation time based on timer from update
var updateEvent = script.createEvent("UpdateEvent");
updateEvent.bind(function (eventData){
    var particleTime = getTime() - startTime;
    script.crumbsMaterial.mainPass.externalTimeInput = particleTime;
});

// Destroy crumbs object after 'lifetime' seconds
var delayedEvent = script.createEvent("DelayedCallbackEvent");
delayedEvent.bind(function (eventData){
    script.crumbsMaterial.mainPass.externalTimeInput = 0;
    script.getSceneObject().destroy();
});
delayedEvent.reset(script.lifetime);