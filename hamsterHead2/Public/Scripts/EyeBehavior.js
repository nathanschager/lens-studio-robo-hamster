// -----JS CODE-----
//@input float blinkInterval
//@input float blinkDuration
//@input float satisfiedDuration
//@input int numMicroblinks
//@input Asset.Material eyeMaterial
//@input Asset.Texture eyeClosedTexture
//@input Asset.Texture eyeOpenTexture
//@input Asset.Texture eyeSatisfiedTexture

// Events
var updateEvent = script.createEvent("UpdateEvent");

// Enum for blinking state machine
const BlinkState = Object.freeze({
    Open:           Symbol("open"),
    BlinkingOpen:   Symbol("blinkingOpen"),
    BlinkingClosed: Symbol("blinkingClosed"),
    Satisfied:      Symbol("satisfied")
});
var blinkState = BlinkState.Open;

// Timer var for various blink events
var blinkTimer = 0;
var microBlinkTimer = 0;
var microBlinkDuration = script.blinkDuration / (2 * script.numMicroblinks);

// On update, increment timer and execute state function
function update(eventData)
{
    blinkTimer += getDeltaTime();
    switch (blinkState)
    {
        case BlinkState.Satisfied:
            stateSatisfied();
            break;
        case BlinkState.BlinkingOpen:
            stateBlinkingOpen();
            break;
        case BlinkState.BlinkingClosed:
            stateBlinkingClosed();
            break;
        default:
            stateOpen();
    }
}
updateEvent.bind(update);

// If blink function times out, open eye again
// Else, close eye if microblink duration reached
function stateBlinkingOpen()
{
    if (blinkTimer > script.blinkDuration)
    {
        setBlinkState(BlinkState.Open, false);
        
    }
    else if (microBlinkTimer > microBlinkDuration)
    {
        setBlinkState(BlinkState.BlinkingClosed, true);
    }
    else
    {
        microBlinkTimer += getDeltaTime();
    }
}

// If blink function times out, open eye again
// Else, open eye if microblink duration reached
function stateBlinkingClosed()
{
    if (blinkTimer > script.blinkDuration)
    {
        setBlinkState(BlinkState.Open, false);
        microBlinkTimer = 0;
    }
    else if (microBlinkTimer > microBlinkDuration)
    {
        setBlinkState(BlinkState.BlinkingOpen, true);
        microBlinkTimer = 0;
    }
    else
    {
        microBlinkTimer += getDeltaTime();
    }
}

// If eye stays open too long, blink
function stateOpen()
{
    if (blinkTimer > script.blinkInterval)
    {
        setBlinkState(BlinkState.BlinkingClosed, false);
    }
}

// If satisfied by food for long enough, open eyes again
function stateSatisfied()
{
    if (blinkTimer > script.satisfiedDuration)
    {
        setBlinkState(BlinkState.Open, false);
        blinkTimer += script.blinkInterval / 2; // speeds up next blink, because it's cute
    }
}

// If a seed is eaten, set eyes to 'satisfied' state
script.api.onSeedEaten = function ()
{
    setBlinkState(BlinkState.Satisfied, false);
}

// Set blink state, reset timers, and set texture
function setBlinkState(state, isMicroblink)
{
    // reset blink and microblink timers
    if (!isMicroblink)
    {
        blinkTimer = 0;
    }
    microBlinkTimer = 0;
    // set blink state
    blinkState = state;
    // set eye texture
    switch (blinkState)
    {
        case BlinkState.Satisfied:
            script.eyeMaterial.mainPass.baseTex = script.eyeSatisfiedTexture;
            break;
        case BlinkState.BlinkingOpen:
            script.eyeMaterial.mainPass.baseTex = script.eyeOpenTexture;
            break;
        case BlinkState.BlinkingClosed:
            script.eyeMaterial.mainPass.baseTex = script.eyeClosedTexture;
            break;
        default:
            script.eyeMaterial.mainPass.baseTex = script.eyeOpenTexture;
    }
}