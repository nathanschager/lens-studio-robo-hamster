// -----JS CODE-----
//@input SceneObject mouthTarget
//@input Component.Camera mainCamera
//@input Component.ScriptComponent eyeBehavior
//@input Component.ScriptComponent seedSpawner
//@input Asset.ObjectPrefab crumbsPrefab
//@input Asset.Material sparkleMaterial
//@input float lifespan = 0
//@input float distThreshold
//@input float bobbleHeight
//@input float bobbleSpeed
//@input float spinSpeed

// Events
var updateEvent = script.createEvent("UpdateEvent");
var mouthClosedEvent = script.createEvent("MouthClosedEvent");
var mouthOpenedEvent = script.createEvent("MouthOpenedEvent");

// Components
var transform = script.getSceneObject().getTransform();

// Clone + set particle material for sparkle mesh visual to keep effect timer local
var sparkleMaterialClone = script.sparkleMaterial.clone();
var sparkleMeshVisual = script.getSceneObject().getChild(0).getFirstComponent("Component.RenderMeshVisual");
sparkleMeshVisual.addMaterial(sparkleMaterialClone);

// Numbers
var lifetime = 0;
var initialPhase = Math.random() * Math.PI * 2; // offset initial phase of bobble for variation
rotateBy(Math.random()); // set initial rotation randomly for variation

// Transform seed, or delete if lifespan is exceeded
function update(eventData)
{
    // Seed lifetime
    if (script.lifespan && lifetime > script.lifespan)
    {
        deleteSeed();
    }
    else
    {
        updateTransform();
        sparkleMaterialClone.mainPass.externalTimeInput = lifetime;
        lifetime += getDeltaTime();
    }
}
updateEvent.bind(update);

// If mouth is opened and seed is close enough, trigger seed eaten logic
// in associated object, spawn crumbs, and delete this seed
function testSeedDistance(eventData)
{
    var dist = getScreenDist();
    if (dist < script.distThreshold)
    {
        print("Seed eaten!");
        script.eyeBehavior.api.onSeedEaten();
        script.crumbsPrefab.instantiate(script.mouthTarget); // create crumbs particles
        script.seedSpawner.api.onSeedEaten();
        deleteSeed();
    }
}
mouthClosedEvent.bind(testSeedDistance); // seems to work best in tests when people can eat 
mouthOpenedEvent.bind(testSeedDistance); // sunflower seeds with open and close mouth events

function getScreenDist()
{
    var seedPos = transform.getWorldPosition();
    var mouthTargetPos = script.mouthTarget.getTransform().getWorldPosition();
    var seedPosScreen = script.mainCamera.worldSpaceToScreenSpace(seedPos);
    var mouthTargetScreen = script.mainCamera.worldSpaceToScreenSpace(mouthTargetPos);
    return seedPosScreen.distance(mouthTargetScreen);
}

// Every update, change transform
function updateTransform()
{
    // Update position with sine bobble
    var pos = transform.getWorldPosition();
    var y = pos.y + Math.sin(initialPhase + getTime() * script.bobbleSpeed) * script.bobbleHeight;
    transform.setWorldPosition(new vec3(pos.x, y, pos.z));
    // Update rotation by spinning over time
    rotateBy(script.spinSpeed * getDeltaTime());
}

// Rotate the seed along the y axis
function rotateBy(amount)
{
    var rotation = transform.getLocalRotation();
    var rotateByAmount = quat.angleAxis(Math.PI * amount, vec3.up());
    rotation = rotation.multiply(rotateByAmount);
    transform.setLocalRotation(rotation);
}

// Delete seed, decrement seed counter
function deleteSeed()
{
    global.numSeeds -= 1;
    script.getSceneObject().destroy();
}